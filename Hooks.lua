dofile(ModPath .. "DevTools.lua")

if not DevTools or not DevTools.setup then
    return
end

if string.lower(RequiredScript) == "lib/entry" then
    -- setup debugmode, pcalls, crashlog, applog

	-- rawset(getmetatable(Application),"debug_enabled",function() return true end)

    local log_original = log
    function log(message)
        log_original(message)
        if DevTools and DevTools.debugmode and managers and managers.gui_data then
            DevTools:print(message)
        end
    end

    -- local NotifyErrorOverlay_original = NotifyErrorOverlay
    -- function NotifyErrorOverlay(message)
    --     if NotifyErrorOverlay_original then
    --         NotifyErrorOverlay_original(message)
    --     end

    --     local file = io.open(LogsPath .. "crash.txt", "w+")
    --     if file then
    --         file:write(tostring(message))
    --         file:close()
    --     end

    --     local logfile = io.open(LogsPath .. "crashlog.txt", "a+")
    --     if logfile then
    --         logfile:write("\n" .. tostring(message) .. "\n\n\n---------------------------------------------------\n")
    --         logfile:close()
    --     end

    --     if DevTools and DevTools.debugmode and managers and managers.gui_data then
    --         DevTools:error(message)
    --     end

    --     if not DevTools or not DevTools.pcalls then
    --         blt.forcepcalls(false)
    --         DelayedCalls:Add("crashme", 1, function()
    --             ---@diagnostic disable-next-line: undefined-global
    --             crashme() -- cause crash on purpose, by calling a function that does not exist
    --         end)
    --     end
    -- end

    -- blt.forcepcalls(true)
    if blt.EnableApplicationLog then
        blt.EnableApplicationLog(DevTools.applog)
    end
elseif string.lower(RequiredScript) == "lib/setups/setup" then
    local init_managers_original = Setup.init_managers

    function Setup:init_managers(...)
        init_managers_original(self, ...)
        DevTools:init_overlay()
    end
elseif string.lower(RequiredScript) == "lib/managers/menu/raid_menu/raidmainmenugui" then
    local init_original = RaidMainMenuGui.init

    function RaidMainMenuGui:init(...)
        init_original(self, ...)
        if managers.network and managers.network.matchmake then
            DevTools:reconnect()
        end
    end
elseif string.lower(RequiredScript) == "lib/network/matchmaking/networkmatchmakingsteam" then
    local join_server_original = NetworkMatchMakingSTEAM.join_server

    function NetworkMatchMakingSTEAM:join_server(room_id, ...)
        join_server_original(self, room_id, ...)
        DevTools:set_room_id(room_id)
    end
end
