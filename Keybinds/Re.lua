
if game_state_machine and Network and DevTools then
	local state = game_state_machine:current_state_name()
	if state == "menu_main" then
		-- main menu
		DevTools:reset_menu()
	elseif state == "ingame_lobby_menu" or
			state == "ingame_waiting_for_respawn" or
			state == "ingame_waiting_for_players" or
			state == "ingame_mask_off" or
			state == "ingame_standard" then
		-- in game
		if Network:is_server() then
			-- as host
			DevTools:yesno(callback(DevTools, DevTools, 'reset_server'))
		else
			-- as client
			DevTools:yesno(callback(DevTools, DevTools, 'reset_client'))
		end
	elseif state == "disconnected" then
		-- do nothing
	end
end
