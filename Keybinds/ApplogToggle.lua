if not blt.EnableApplicationLog then
    DevTools:error("current BLT dll does not support EnableApplicationLog()")
    return
end

if DevTools then
    DevTools.applog = not DevTools.applog
    blt.EnableApplicationLog(DevTools.applog)
    DevTools:print("applog mode: " .. (DevTools.applog and "on" or "off"))
end
