log("< ===== QUICK KEY PRESSED ==========")

-- local CURRENT_WEAPON = managers.player:local_player():inventory():equipped_unit():base():get_name_id() or "nil"
-- log("current weapon: " .. CURRENT_WEAPON)

-- add gold
--[[ if managers.gold_economy and managers.savefile then
	managers.gold_economy:add_gold(500)
	--managers.gold_economy:spend_gold(500) -- remove gold
	managers.savefile:set_resave_required()
end ]]

-- add xp
--[[ if managers.experience then
	managers.experience:add_points(100000, false)
end ]]

-- unlock weapon challenges
--[[ if managers.challenge then
	local unlocked_smth = false
	for i = 1, 4, 1 do -- for each tier
		for challenge_id, challenge in pairs(managers.challenge._challenges.weapon_upgrade) do
			-- challenge is for current weapon and tier, and not completed...
			if challenge._data.weapon == CURRENT_WEAPON and challenge._data.tier == i and not challenge:completed() then
				challenge:force_complete() -- sets all tasks to completed and removes the challenge listener
				log("Force completed challenge: " .. tostring(challenge_id) .. " for weapon: " .. CURRENT_WEAPON)
				challenge:on_task_completed() -- validates above command and triggers the complation callback, which is:
												-- managers.weapon_skills:on_weapon_challenge_completed()
												-- therefore popups a notifiation and triggers achievements like '(fully) upgrade a weapon'
				unlocked_smth = true
				break
			end
		end
		if unlocked_smth then
			break
		end
	end
	if not unlocked_smth then
		log("All upgrades are unlocked for his weapon already")
	end
end ]]

-- unlock all missions
--[[ if managers.progression then
	managers.progression:_unlock_all_missions()
end ]]

-- unlock all consumable missions
--[[ for key, raid in pairs(tweak_data.operations:get_all_consumable_raids()) do
	managers.consumable_missions:_unlock_mission(raid)
end ]]

-- complete all missions
--[[ for key, raid in pairs(tweak_data.operations:get_raids_index()) do
	managers.progression:complete_mission_on_difficulty(OperationsTweakData.JOB_TYPE_RAID, raid, 4)
end ]]

-- TODO: complete all operations ?
--"clear_skies",
--"oper_flamable"

--fake daily duty notification
-- local reward_data = tweak_data.events.active_duty_bonus_rewards[managers.event_system._consecutive_logins]
-- --local reward = EventsTweakData.REWERD_TYPE_GOLD
-- local notification_params = {
-- 	name = "active_duty_bonus",
-- 	priority = 4,
-- 	notification_type = "active_duty_bonus",
-- 	duration = 13,
-- 	icon = reward_data.icon,
-- 	consecutive = managers.event_system._consecutive_logins,
-- 	total = #tweak_data.events.active_duty_bonus_rewards
-- }
-- notification_params.amount = tweak_data.events.active_duty_bonus_rewards[managers.event_system._consecutive_logins].amount
-- managers.notification:add_notification(notification_params)

-- fake consumoable mission picked up notification
-- local chosen_consumable = tweak_data.operations:get_random_unowned_consumable_raid()
-- managers.consumable_missions:pickup_mission(chosen_consumable)
-- local notification_data = {
-- 	id = "hud_hint_consumable_mission",
-- 	doc_text = "hud_hint_consumable_mission_secured",
-- 	doc_icon = "notification_consumable",
-- 	duration = 4,
-- 	priority = 3,
-- 	notification_type = HUDNotification.CONSUMABLE_MISSION_PICKED_UP
-- }
-- managers.notification:add_notification(notification_data)

--fake greed item notification
-- managers.notification:add_notification({
-- 	id = "greed_item_picked_up",
-- 	shelf_life = 8,
-- 	notification_type = HUDNotification.GREED_ITEM,
-- 	initial_progress = 0.8,
-- 	new_progress = 0.9,
-- 	item = tweak_data.greed.greed_items.egg_decoration
-- })

-- managers.hud:present_mid_text({
-- 	time = 4,
-- 	text = "mjeidofwshJ eg hnbUAENKlk ehnuifadlkrn igzsdbniju orf ghn iozader ghni o;adshbn ioedr/g\nawhn'eoipt bho;i9w4 bnthoi; aWERHO'I8G HB;OISRE YHJI'OPAE4 HJOIYNBAHEIO;45RY\nJHO9PU'AE45 HOYI9AQ4E5H;OIYH IO/L",
-- 	title = title,
-- 	icon = icon
-- })

-- PlayerInfo debug
-- for id = 1, 4 do
-- 	local info = PlayerInfo:GetPlayerInfoAll()[id]
-- 	log(tostring(info.name) .. " (" .. tostring(info.class) .. ")")
-- 	PrintTable(info.skills, 10)
-- end

-- PrintTable(PlayerInfo.upgrade_to_skills_with_level_index)
-- log(tostring(managers.raid_job:current_job_type()))
-- log(tostring(managers.raid_job:current_job_id()))
-- log(tostring(managers.raid_job:current_operation_event().level_id))
-- --PrintTable(managers.raid_job:current_job(), 10)
--log(Global.game_settings.level_id)

-- log("managers.lootdrop count (_loot_spawned_current_leg): " .. tostring(managers.lootdrop._loot_spawned_current_leg))
-- local dogtags = 0
-- local inwg = {}
-- for _, unit in ipairs(managers.interaction._interactive_units) do
-- 	if unit:interaction().tweak_data == "press_take_dogtags" then
-- 		dogtags = dogtags + unit:loot_drop():value()
-- 		inwg[unit:id()] = unit
-- 	end
-- end
-- log("managers.interaction count (_interactive_units): " .. tostring(dogtags))

-- dogtags = 0 --
-- local invanilla = {}
-- for _, unit in ipairs(managers.lootdrop._active_loot_units) do
-- 	if alive(unit) --[[and unit.loot_drop and unit:loot_drop().value]] then
-- 		dogtags = dogtags + unit:loot_drop():value()
-- 		if not inwg[unit:id()] then
-- 			log("where does this come from? managers.interaction does not have this one!: " .. tostring(unit))
-- 		end
-- 		invanilla[unit:id()] = unit
-- 	else
-- 		log("DEAD UNIT IN _active_loot_units !: " .. tostring(unit))
-- 	end
-- end
-- for k in pairs(inwg) do
-- 	if not invanilla[k] then
-- 		log('detected interactable dogtag in managers.interaction, but not in managers.lootdrop!: ' .. tostring(inwg[k]))
-- 	end
-- end

-- log("managers.lootdrop count (without the null units mentioned above) : " .. tostring(dogtags))

-- -- spawn rogues gallery mission
-- if managers.player and managers.raid_job and managers.player:local_player_in_camp() then
-- 	managers.raid_job._next_event_index = nil
-- 	managers.raid_job:set_selected_job("sto")
-- end

-- local notification_params = {
-- 	sound_effect = "daily_login_reward",
-- 	id = "beardlib_custom_achievement_TEST",
-- 	duration = 5,
-- 	shelf_life = 5,
-- 	notification_type = HUDNotification.CUSTOM_ACHIEVEMENT,
-- 	text = managers.localization:to_upper_text("beardlib_achieves_achieved") .. '\n' .. "PENIS",
-- 	icon = "guis/textures/achievement_trophy_white",
-- 	icon_color = Color("CD7F32")
-- }
-- managers.notification:add_notification(notification_params)

log("< ===== QUICK KEY END ==============")
