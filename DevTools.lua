_G.DevTools = _G.DevTools or {}

if not DevTools.setup then
	DevTools.setup = true

	-- reconnect cache
	DevTools._room_file = SavePath .. "LastRoomID.txt"

	-- debug toggle
	DevTools.debugmode = true
	DevTools.applog = false
	-- DevTools.pcalls = false

	-- overlay tweaks
	DevTools.MAX_OUTPUTS = 20
	DevTools.MARGIN = 2
	DevTools.LINES_HEIGHT = 16

	function DevTools:init_overlay()
		self._outputs = {}
		self._setup = true
		self:_setup_overlay()
	end

	function DevTools:_setup_overlay()
		if not self._setup then
			return
		end

		--if not self._reschanged_clbk_id then
		--	self._reschanged_clbk_id = managers.viewport:add_resolution_changed_func(callback(self, self, "_setup_overlay"))
		--end

		if not alive(self._ws) then
			self._ws = managers.gui_data:create_fullscreen_workspace()
			self._panel = self._ws:panel()
		else
			self._panel:clear()
		end

		self._panel:panel({
			name = "output_panel",
			layer = 1000000,
			x = self._panel:w() * 0.67,
			y = self._panel:h() * 0.45,
			w = self._panel:w() * 0.25,
			h = self._panel:h() * 0.33,
			valign = "top",
		})

		self:_layout_overlay()
	end

	function DevTools:_layout_overlay()
		if not self._setup then
			return
		end

		local output_panel = self._panel:child("output_panel")
		output_panel:clear()

		local bottom = output_panel:h()
		local current = 0
		while bottom >= 0 and current < #self._outputs do
			local text_data = self._outputs[#self._outputs - current]
			local text = text_data.text ~= "" and text_data.text or " " -- prevent zero height at text_rect()
			local line = output_panel:text({
				layer = 0,
				w = output_panel:w(),
				align = "left",
				vertical = "top",
				halign = "left",
				hvertical = "top",
				wrap = true,
				word_wrap = true,
				blend_mode = "normal",
				text = (text_data.time and text_data.time .. " > " or "") .. text,
				font = tweak_data.gui.fonts.din_compressed_outlined_20,
				font_size = DevTools.LINES_HEIGHT,
				color = text_data.color
			})

			local _, _, _, h = line:text_rect()
			line:set_h(h)
			line:set_bottom(bottom)
			bottom = line:y()

			current = current + 1
		end
	end

	function DevTools:destroy_overlay()
		if not self._setup then
			return
		end
		--if self._reschanged_clbk_id then
		--	managers.viewport:remove_resolution_changed_func(self._reschanged_clbk_id)
		--	self._reschanged_clbk_id = nil
		--end
		if alive(self._ws) then
			managers.gui_data:destroy_workspace(self._ws)
			self._ws = nil
			self._panel = nil
		end
	end

	function DevTools:_add_text(text, color, show_time)
		if not self._setup then
			return
		end
		if not alive(self._ws) then
			self:_setup_overlay()
		end
		local texts = text:split("\n", true)
		local n = #texts
		for i = 1, n do
			local formatted_text = texts[i]:gsub("\t", "    ")
			local text_data = {
				time = show_time and os.date("%X") or nil,
				text = formatted_text,
				color = color and color or Color.white:with_alpha(0.6)
			}
			show_time = nil -- only show time in first line
			table.insert(self._outputs, text_data)
			while #self._outputs >= DevTools.MAX_OUTPUTS do
				table.remove(self._outputs, 1)
			end
		end
		self:_layout_overlay()
		self._panel:stop()
		self._panel:animate(callback(self, self, "_animate_fade_out"))
	end

	function DevTools:_animate_fade_out(panel)
		local sustain = 3
		local release = 0.5
		local t = sustain + release
		panel:set_alpha(1)
		while t > 0 do
			t = t - coroutine.yield()
			if t <= release then
				panel:set_alpha(math.clamp(t / release, 0, 1))
			end
		end
		panel:set_alpha(0)
	end

	function DevTools:error(message)
		if not self._setup then
			return
		end
		self:_add_text(message, Color.red, false)
	end

	function DevTools:print(...)
		if not self._setup then
			return
		end
		local r
		local n = select("#", ...)
		for i = 1, n do
			local v = select(i, ...)
			local t = v and tostring(v) or "[nil]"
			if r and v and t ~= "\n" and t ~= "\t" then
				r = r .. "\t" .. t
			elseif r then
				r = r .. t
			else
				r = t
			end
		end
		self:_add_text(r, Color.white, false)
	end

	function DevTools:printf(str, ...)
		if not self._setup then
			return
		end
		self:print(str:format(...))
	end

	function DevTools:yesno(clbk)
		local localization = managers.localization
		QuickMenu:new(
			localization:text("dialog_warning_title"),
			localization:text("dialog_are_you_sure_you_want_leave"),
			{
				{ text = localization:text("dialog_yes"), callback = clbk },
				{ text = localization:text("dialog_no"),  is_cancel_button = true }
			},
			true
		)
	end

	function DevTools:reset_menu()
		if setup and setup.quit_to_main_menu then
			setup.exit_to_main_menu = true
			setup:quit_to_main_menu() -- Tested OK
		end
	end

	function DevTools:reset_server()
		local all_synced = true
		for _, v in pairs(managers.network:session():peers()) do
			if not v:synched() then
				all_synced = false
			end
		end
		if all_synced then
			-- Restart
			if not game_state_machine then
				do return end
			end
			local state = game_state_machine:current_state_name()
			if state == "ingame_waiting_for_players" or
				state == "ingame_lobby_menu" or
				state == "empty" then
				do return end
			end
			local job = managers.raid_job
			if not job or job:is_in_tutorial() or (not job:is_camp_loaded() and job:current_job() and job:current_job().consumable) then
				do return end
			end
			--if job:is_camp_loaded() then -- FIXME: implement else block with lua reload
			if Global.game_settings.single_player then
				log("reload camp SP")
				RaidMenuCallbackHandler:raid_play_offline() -- Tested OK
			elseif Network:is_server() then
				log("reload camp MP")
				if setup and setup.quit_to_main_menu then -- Tested OK
					Global.game_settings.single_player = false
					Global.exe_argument_level = "streaming_level"
					Global.exe_argument_difficulty = Global.exe_argument_difficulty or Global.DEFAULT_DIFFICULTY
					setup.exit_to_main_menu = true
					setup:quit_to_main_menu()
				end
			end
			--[[else
				if Global.game_settings.single_player then
					log("restart mission SP")
					managers.game_play_central:restart_the_mission() -- FIXME: Tested OK, not reloading lua code though
				elseif Network:is_server() then
					log("restart mission MP")
					managers.vote:restart_mission_auto() -- FIXME: Tested OK, not reloading lua code though
				end
			end]]
			managers.raid_menu:on_escape()
		else
			managers.chat:_receive_message(1, "[DevTools]", "Cannot restart, a player may be loading.", Color.red)
		end
	end

	function DevTools:set_room_id(room_id)
		Global.dev_tools_room_id = room_id
	end

	function DevTools:reset_client()
		if Global.dev_tools_room_id then
			-- Save Room ID
			local file = io.open(self._room_file, "w")
			if file then
				file:write(tostring(Global.dev_tools_room_id))
				file:close()
			end
			-- Disconnect
			MenuCallbackHandler:_dialog_end_game_yes()
		else
			managers.chat:_receive_message(1, "[DevTools]", "Wot?!", Color.red)
		end
	end

	function DevTools:reconnect()
		-- Reconnect
		local file = io.open(self._room_file, "r")
		if file then
			local room_id = file:read("*all")
			file:close()
			SystemFS:delete_file(self._room_file)
			managers.network.matchmake:join_server(room_id)
		end
	end
end
